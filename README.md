# Klaros

## short version :

This C++ program offers the possibility to control a Dyson Pure Cool 2018 device and get infos of it.

## long version :

Dyson offers several devices which use a mosquitto server to communicate with the Android/iOS app.

They do not offer an api but it is possible to sniff the app messages to get the needed access data and function syntax. Keep in mind that the password is a SHA256 hashed value with 88 signs length.

There are already libraries and snippets for example [written in python](https://www.home-assistant.io/integrations/dyson/) for home assistant system to support such devices.

This software shows how you can control and get infos of a Dyson Pure Cool 2018 device in C++. To compile it you need gcc, cmake RapidJSON and MQTT-C/MQTT-C++ libraries.

In .gitlab-ci.yml you can see how you can build it in Linux.

It is also possible to build it with Microsoft Visual Studio. In this case you have to install cmake at first. After that you have to download and build MQTT-C/MQTT-C++ libraries.
I recomme to install the results to some directory and set enviroment variables MQTT-C_DIR/MQTT-C++_DIR to these paths. This is used in CMakeModules/FindMQTT-C.cmake respectively CMakeModules/FindMQTT-C++.cmake.
Of course you can also use prebuilt libraries.
RapidJSON is a header only library.

- cmake : [https://cmake.org/download/](https://cmake.org/download/)
- MQTT-C : [https://www.eclipse.org/paho/clients/c](https://www.eclipse.org/paho/clients/c) or [https://github.com/eclipse/paho.mqtt.c](https://github.com/eclipse/paho.mqtt.c)
- MQTT-C++ : [https://www.eclipse.org/paho/clients/cpp](https://www.eclipse.org/paho/clients/cpp) or [https://github.com/eclipse/paho.mqtt.cpp](https://github.com/eclipse/paho.mqtt.cpp)
- RapidJSON : [https://rapidjson.org/](https://rapidjson.org/)

After that you can use cmake to generate Visual Studio solution structure and build Klaros.

After successful build you can start the software. 3 arguments are needed for that :
- ip address of dyson device (take a look at your router)
- username (by sniffing your mobile app)
- password (by sniffing your mobile app)

So you start it by :
```klaros-cmd <ip address> <username> <password>```

After that you can enter commands. To show available commands enter help.

## Gui

There is also a gui application which bases on the same core communication system.
To build it you need additionally the QT library. In Linux you can install it this by
```sudo apt install qttools5-dev```
For Windows you can also download prebuilt QT library or the source code. [Link](https://www.qt.io/download)

After successfully build you can start it by
```klaros-gui```
You can enter the login data in the window.
Optional you can also add this data by 3 arguments like in klaros-cmd
```klaros-gui [ip address] [username] [password]```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Author
Silvio Tristram

## License
Copyright (C) 2020, [Silvio Tristram](https://gitlab.com/Amhehu), released under [GPL 3](https://gitlab.com/Amhehu/klaros/-/blob/master/LICENSE)

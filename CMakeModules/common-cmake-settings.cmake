# Klaros
# Copyright (C) 2020 Silvio Tristram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

IF (MSVC)

  UNSET(CMAKE_C_FLAGS_MINSIZEREL                 CACHE)
  UNSET(CMAKE_C_FLAGS_RELWITHDEBINFO             CACHE)
  UNSET(CMAKE_CXX_FLAGS_MINSIZEREL               CACHE)
  UNSET(CMAKE_CXX_FLAGS_RELWITHDEBINFO           CACHE)
  UNSET(CMAKE_EXE_LINKER_FLAGS_MINSIZEREL        CACHE)
  UNSET(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO    CACHE)
  UNSET(CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL     CACHE)
  UNSET(CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO CACHE)
  UNSET(CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL     CACHE)
  UNSET(CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO CACHE)
  UNSET(CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL     CACHE)
  UNSET(CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO CACHE)

  SET(CMAKE_C_FLAGS   "/DWIN32 /D_WINDOWS /W3"       CACHE STRING "set general C compile flags"   FORCE)
  SET(CMAKE_CXX_FLAGS "/DWIN32 /D_WINDOWS /W3 /EHsc" CACHE STRING "set general C++ compile flags" FORCE)

  SET(CMAKE_C_FLAGS_DEBUG     "/MDd /Od /Ob0 /D_DEBUG /RTC1 /Zi" CACHE STRING "set debug C compile flags"     FORCE)
  SET(CMAKE_CXX_FLAGS_DEBUG   "/MDd /Od /Ob0 /D_DEBUG /RTC1 /Zi" CACHE STRING "set debug C++ compile flags"   FORCE)
  SET(CMAKE_C_FLAGS_RELEASE   "/MD  /Ox /Ob2 /DNDEBUG"           CACHE STRING "set release C compile flags"   FORCE)
  SET(CMAKE_CXX_FLAGS_RELEASE "/MD  /Ox /Ob2 /DNDEBUG"           CACHE STRING "set release C++ compile flags" FORCE)

ELSEIF (CMAKE_COMPILER_IS_GNUCXX)

  SET(CMAKE_THREAD_PREFER_PTHREAD ON)
  FIND_PACKAGE(Threads REQUIRED)
  ADD_DEFINITIONS(-Wall -Wno-unknown-pragmas)
  SET(CMAKE_C_FLAGS   "-O3 " CACHE STRING "set C compile flags"   FORCE)
  SET(CMAKE_CXX_FLAGS "-O3 " CACHE STRING "set C++ compile flags" FORCE)

ELSE ()

  MESSAGE("compile flags were not set, because of unknown compiler")

ENDIF ()

SET (CMAKE_CXX_STANDARD 20)

SET_PROPERTY (GLOBAL PROPERTY FIND_LIBRARY_USE_LIB64_PATHS ON     )
SET_PROPERTY (GLOBAL PROPERTY USE_FOLDERS                  ON     )
SET_PROPERTY (GLOBAL PROPERTY DEBUG_CONFIGURATIONS         "Debug")

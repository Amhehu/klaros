# Klaros
# Copyright (C) 2020 Silvio Tristram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

CMAKE_MINIMUM_REQUIRED (VERSION 3.0.0)

IF (MQTT_C_FOUND)
  INCLUDE_DIRECTORIES (SYSTEM ${MQTT_C_INCLUDE_DIR})

  IF (MQTT_C++_INCLUDE_DIR AND MQTT_C++_LIBRARIES)
    SET (MQTT_C++_FIND_QUIETLY TRUE)
  ENDIF (MQTT_C++_INCLUDE_DIR AND MQTT_C++_LIBRARIES)

  FIND_PATH (MQTT_C++_INCLUDE_DIR NAMES mqtt/client.h
    PATHS
      $ENV{MQTT-C++_DIR}
      $ENV{MQTT-C++_INCLUDE_DIR}
    PATH_SUFFIXES
      include
  )

  FIND_LIBRARY (MQTT_C++_LIBRARY_DEBUG NAMES paho-mqttpp3-debug-static paho-mqttpp3
    PATHS
      $ENV{MQTT-C++_DIR}
      $ENV{MQTT-C++_LIB_DIR}
    PATH_SUFFIXES
      lib
  )

  FIND_LIBRARY (MQTT_C++_LIBRARY_RELEASE NAMES paho-mqttpp3-release-static paho-mqttpp3
    PATHS
      $ENV{MQTT-C++_DIR}
      $ENV{MQTT-C++_LIB_DIR}
    PATH_SUFFIXES
      lib
  )

  IF (MQTT_C++_LIBRARY_DEBUG)
    SET (MQTT_C++_LIBRARIES_TEMP debug ${MQTT_3A-C_LIBRARY_DEBUG} debug ${MQTT_3C-C_LIBRARY_DEBUG} debug ${MQTT_C++_LIBRARY_DEBUG})
  ENDIF (MQTT_C++_LIBRARY_DEBUG)

  IF (MQTT_C++_LIBRARY_RELEASE)
    SET (MQTT_C++_LIBRARIES_TEMP optimized ${MQTT_3A-C_LIBRARY_RELEASE} optimized ${MQTT_3C-C_LIBRARY_RELEASE} optimized ${MQTT_C++_LIBRARY_RELEASE} ${MQTT_C++_LIBRARIES_TEMP})
  ENDIF (MQTT_C++_LIBRARY_RELEASE)

  IF(MQTT_C++_LIBRARIES_TEMP)
    SET (MQTT_C++_LIBRARIES ${MQTT_C++_LIBRARIES_TEMP} ${CMAKE_THREAD_LIBS_INIT} CACHE STRING "MQTT_C++ Bibliotheken")
  ENDIF(MQTT_C++_LIBRARIES_TEMP)

  IF (MQTT_C++_INCLUDE_DIR AND MQTT_C++_LIBRARIES)
    SET (MQTT_C++_FOUND TRUE)
  ELSE (MQTT_C++_INCLUDE_DIR AND MQTT_C++_LIBRARIES)
    SET (MQTT_C++_FOUND FALSE)
  ENDIF (MQTT_C++_INCLUDE_DIR AND MQTT_C++_LIBRARIES)

ENDIF (MQTT_C_FOUND)

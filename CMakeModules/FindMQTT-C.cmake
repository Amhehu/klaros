# Klaros
# Copyright (C) 2020 Silvio Tristram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

CMAKE_MINIMUM_REQUIRED (VERSION 3.0.0)

IF (MQTT_C_INCLUDE_DIR AND MQTT_C_LIBRARY)
  SET (MQTT_C_FIND_QUIETLY TRUE)
ENDIF (MQTT_C_INCLUDE_DIR AND MQTT_C_LIBRARY)

FIND_PATH (MQTT_C_INCLUDE_DIR NAMES MQTTClient.h
  PATHS
    $ENV{MQTT-C_DIR}
    $ENV{MQTT-C_INCLUDE_DIR}
    /usr/local
  PATH_SUFFIXES
    include
)

FIND_LIBRARY (MQTT_3A-C_LIBRARY_DEBUG NAMES paho-mqtt3a-debug paho-mqtt3a
  PATHS
    $ENV{MQTT-C_DIR}
    $ENV{MQTT-C_LIB_DIR}
    /usr/local
  PATH_SUFFIXES
    lib
)

FIND_LIBRARY (MQTT_3C-C_LIBRARY_DEBUG NAMES paho-mqtt3c-debug paho-mqtt3c
  PATHS
    $ENV{MQTT-C_DIR}
    $ENV{MQTT-C_LIB_DIR}
    /usr/local
  PATH_SUFFIXES
    lib
)

FIND_LIBRARY (MQTT_3A-C_LIBRARY_RELEASE NAMES paho-mqtt3a-release paho-mqtt3a
  PATHS
    $ENV{MQTT-C_DIR}
    $ENV{MQTT-C_LIB_DIR}
    /usr/local
  PATH_SUFFIXES
    lib
)

FIND_LIBRARY (MQTT_3C-C_LIBRARY_RELEASE NAMES paho-mqtt3c-release paho-mqtt3c
  PATHS
    $ENV{MQTT-C_DIR}
    $ENV{MQTT-C_LIB_DIR}
    /usr/local
  PATH_SUFFIXES
    lib
)

IF (MQTT_3A-C_LIBRARY_DEBUG)
  SET (MQTT_C_LIBRARIES_TEMP ${MQTT_C_LIBRARIES_TEMP} debug ${MQTT_3A-C_LIBRARY_DEBUG})
ENDIF (MQTT_3A-C_LIBRARY_DEBUG)

IF (MQTT_3C-C_LIBRARY_DEBUG)
  SET (MQTT_C_LIBRARIES_TEMP ${MQTT_C_LIBRARIES_TEMP} debug ${MQTT_3C-C_LIBRARY_DEBUG})
ENDIF (MQTT_3C-C_LIBRARY_DEBUG)

IF (MQTT_3A-C_LIBRARY_RELEASE)
  SET (MQTT_C_LIBRARIES_TEMP ${MQTT_C_LIBRARIES_TEMP} optimized ${MQTT_3A-C_LIBRARY_RELEASE})
ENDIF (MQTT_3A-C_LIBRARY_RELEASE)

IF (MQTT_3C-C_LIBRARY_RELEASE)
  SET (MQTT_C_LIBRARIES_TEMP ${MQTT_C_LIBRARIES_TEMP} optimized ${MQTT_3C-C_LIBRARY_RELEASE})
ENDIF (MQTT_3C-C_LIBRARY_RELEASE)

IF(MQTT_C_LIBRARIES_TEMP)
  SET (MQTT_C_LIBRARIES ${MQTT_C_LIBRARIES_TEMP} ${CMAKE_THREAD_LIBS_INIT} CACHE STRING "MQTT_C Bibliotheken")
ENDIF(MQTT_C_LIBRARIES_TEMP)

IF (MQTT_C_INCLUDE_DIR AND MQTT_C_LIBRARIES)
  SET (MQTT_C_FOUND TRUE)
ELSE (MQTT_C_INCLUDE_DIR AND MQTT_C_LIBRARIES)
  SET (MQTT_C_FOUND FALSE)
ENDIF (MQTT_C_INCLUDE_DIR AND MQTT_C_LIBRARIES)

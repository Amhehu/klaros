// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <gui/gui.hpp>

#include "base/time-point.hpp"

int32_t main(int32_t argc_, char * argv_[])
{
  auto pure_cool_qt_ = std::make_shared<PureCoolQt>();

  pure_cool_qt_->set_client_id(std::string("dyson-fan-") + TimePoint::get_now_as_string());

  if (argc_ > 1)
  {
    pure_cool_qt_->set_server_address(std::string(argv_[1]));

    if (argc_ > 2)
    {
      pure_cool_qt_->set_username(std::string(argv_[2]));

      if (argc_ > 3)
      {
        pure_cool_qt_->set_password(std::string(argv_[3]));
      }
    }
  }

  Gui gui(pure_cool_qt_, argc_, argv_);

  qRegisterMetaType<int32_t>("int32_t");

  return gui.exec();
}

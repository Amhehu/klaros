// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>

#include <controller/pure-cool-interface.hpp>

#include "base/time-point.hpp"

int32_t main(int32_t argc_, char * argv_[])
{
  if (argc_ == 4)
  {
    std::string server_address(argv_[1]);
    std::string username      (argv_[2]);
    std::string password      (argv_[3]);

    auto client_id = std::string("dyson-fan-") + TimePoint::get_now_as_string();

    PureCoolInterface _pure_cool_interface;

    _pure_cool_interface.set_client_id     (client_id     );
    _pure_cool_interface.set_password      (password      );
    _pure_cool_interface.set_server_address(server_address);
    _pure_cool_interface.set_username      (username      );

    _pure_cool_interface.create_client();
    _pure_cool_interface.subscribe_status();

    std::this_thread::sleep_for(std::chrono::milliseconds(1'000));

    bool stop = false;
    std::string input;

    std::cout << "you can quit by entering \"quit\" or \"q\"" << std::endl;
    std::cout << "show available commands by entering \"help\" or \"h\"" << std::endl;

    do
    {
      std::cout << "enter command : ";
      std::getline(std::cin, input);

      if ((input.compare(std::string("quit")) != 0) && (input.compare(std::string("q")) != 0))
      {
        _pure_cool_interface.command(input);
      }
      else
      {
        stop = true;
      }

    } while (stop == false);

    return 0;
  }
  else
  {
    std::cout << "to use this software start it as follows : klaros <ip address> <username> <password>" << std::endl;
    return -1;
  }
}

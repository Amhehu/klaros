# Klaros
# Copyright (C) 2020 Silvio Tristram
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

CMAKE_MINIMUM_REQUIRED (VERSION 3.0.0)

IF (Qt5Widgets_FOUND)
  SET (CMAKE_AUTOMOC ON)

  SET (LIBRARY_NAME gui)
  SET (LIBRARY_SRC
    gui.hpp              gui.cpp
    label-value.hpp      label-value.cpp
    pure-cool-qt.hpp     pure-cool-qt.cpp
    slider-value.hpp     slider-value.cpp
    tab-connection.hpp   tab-connection.cpp
    tab-device-state.hpp tab-device-state.cpp
    tab-interface.hpp    tab-interface.cpp
    tab-sensor-data.hpp  tab-sensor-data.cpp
  )

  ADD_LIBRARY(${LIBRARY_NAME} ${LIBRARY_SRC})

  SET_PROPERTY(TARGET ${LIBRARY_NAME} PROPERTY FOLDER "libraries")

  TARGET_LINK_LIBRARIES(${LIBRARY_NAME} Qt5::Widgets controller)
ENDIF (Qt5Widgets_FOUND)

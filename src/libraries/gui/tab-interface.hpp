// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <memory>

#pragma warning(push, 0)
#include <QWidget>
#pragma warning(pop)

#include "pure-cool-qt.hpp"

class TabInterface : public QWidget
{
  Q_OBJECT

public :
  TabInterface(std::shared_ptr<PureCoolQt> pure_cool_qt_,
               QWidget                   * parent_ = nullptr);

protected :
  std::shared_ptr<PureCoolQt> _pure_cool_qt;

private :
  TabInterface            (const TabInterface & other_) = delete;
  TabInterface & operator=(const TabInterface & other_) = delete;
};

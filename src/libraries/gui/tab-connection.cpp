// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <QBoxLayout>
#include <QPushButton>

#include "tab-connection.hpp"

TabConnection::TabConnection(std::shared_ptr<PureCoolQt> pure_cool_qt_,
                             QWidget                   * parent_) :
  TabInterface(pure_cool_qt_, parent_)
{
  auto main_layout = new QVBoxLayout;
  setLayout(main_layout);

  auto connection_setting_layout = new QFormLayout;
  main_layout->addLayout(connection_setting_layout);

  const mqtt::connect_options & connection_options = _pure_cool_qt->get_connection_options();

  _add_setting(connection_setting_layout, QString("server address"), QString::fromStdString(_pure_cool_qt->    get_server_address()), false, &PureCoolQt::slot_set_server_address);
  _add_setting(connection_setting_layout, QString("username"      ), QString::fromStdString(connection_options.get_user_name     ()), false, &PureCoolQt::slot_set_username      );
  _add_setting(connection_setting_layout, QString("password"      ), QString::fromStdString(connection_options.get_password_str  ()), true , &PureCoolQt::slot_set_password      );
  _add_setting(connection_setting_layout, QString("client id"     ), QString::fromStdString(_pure_cool_qt->    get_client_id     ()), false, &PureCoolQt::slot_set_client_id     );

  auto button_connect = new QPushButton(QString("connect"), this);
  main_layout->addWidget(button_connect);
  connect(button_connect     , &QPushButton::pressed                   , _pure_cool_qt.get(), &PureCoolQt ::slot_connect);
  connect(_pure_cool_qt.get(), &PureCoolQt ::signal_connection_possible, button_connect     , &QPushButton::setEnabled  );
}

QLineEdit * TabConnection::_add_setting(QFormLayout              * layout_,
                                        const QString            & title_,
                                        const QString            & value_,
                                        bool                       hidden,
                                        PureCoolQt::FunctionString function_pointer_slot_)
{
  auto line_edit = new QLineEdit(value_, this);
  line_edit->setEchoMode(hidden == false ? QLineEdit::EchoMode::Normal : QLineEdit::EchoMode::Password);
  layout_->addRow(title_, line_edit);
  connect(line_edit, &QLineEdit::textEdited, _pure_cool_qt.get(), function_pointer_slot_);
  return line_edit;
}

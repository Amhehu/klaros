// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>
#include <vector>

#pragma warning(push, 0)
#include <QSlider>
#pragma warning(pop)

class SliderValue : public QSlider
{
  Q_OBJECT

public :
  SliderValue(const std::vector<int32_t> & valid_values_,
              Qt::Orientation              orientation_,
              QWidget                    * parent_ = nullptr);

signals :
  void signal_value(int32_t value_);

public slots :
  void slot_value(int32_t value_);

protected :
  void keyPressEvent(QKeyEvent * event_) override;

protected slots :
  void _step_changed();

private :
  SliderValue            (const SliderValue & other_) = delete;
  SliderValue & operator=(const SliderValue & other_) = delete;

  std::vector<int32_t> _valid_values = std::vector<int32_t>();
};

// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <QLabel>

class LabelValue : public QLabel
{
  Q_OBJECT

public :
  using QLabel::QLabel;

public slots :
  void set_value(bool                value_);
  void set_value(int32_t             value_);
  void set_value(const std::string & value_);

private :
  LabelValue            (const LabelValue & other_) = delete;
  LabelValue & operator=(const LabelValue & other_) = delete;
};

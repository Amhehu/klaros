// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#pragma warning(push, 0)
#include <QGridLayout>
#pragma warning(pop)

#include "tab-interface.hpp"

class TabDeviceState : public TabInterface
{
  Q_OBJECT

public :
  TabDeviceState(std::shared_ptr<PureCoolQt> pure_cool_qt_,
                 QWidget                   * parent_ = nullptr);

protected :
  void _add_bool_entry(QGridLayout            * layout_,
                       const std::string      & topic_,
                       PureCoolQt::Function     function_slot_on_,
                       PureCoolQt::Function     function_slot_off_,
                       PureCoolQt::FunctionBoolConst function_signal_on_);

  void _add_step_entry(QGridLayout                * layout_,
                       const std::string          & topic_,
                       const QString              & label_lower_,
                       const QString              & label_higher_,
                       PureCoolQt::Function         function_slot_lower_,
                       PureCoolQt::Function         function_slot_higher_,
                       PureCoolQt::FunctionIntConst function_signal_value_);

  void _add_slider_entry(QGridLayout                * layout_,
                         const std::string          & topic_,
                         PureCoolQt::FunctionInt      function_slot_value_,
                         PureCoolQt::FunctionIntConst function_signal_value_,
                         const std::vector<int32_t> & valid_values_);

private :
  TabDeviceState            (const TabDeviceState & other_) = delete;
  TabDeviceState & operator=(const TabDeviceState & other_) = delete;
};

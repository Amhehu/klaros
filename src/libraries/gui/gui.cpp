// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "gui.hpp"
#include "tab-connection.hpp"
#include "tab-device-state.hpp"
#include "tab-sensor-data.hpp"

Gui::Gui(std::shared_ptr<PureCoolQt> pure_cool_qt_, int32_t argc_, char * argv_[]) :
  QApplication(argc_, argv_),
  _main_widget()
{
  _main_widget.addTab(new TabConnection (pure_cool_qt_), QString("connection"  ));
  _main_widget.addTab(new TabDeviceState(pure_cool_qt_), QString("device state"));
  _main_widget.addTab(new TabSensorData (pure_cool_qt_), QString("sensor data" ));

  _slot_set_tabs_enabled(false);

  connect(pure_cool_qt_.get(), &PureCoolQt::signal_connection_status_changed, this, &Gui::_slot_set_tabs_enabled);

  pure_cool_qt_->slot_connect();

  _main_widget.show();
}

void Gui::_slot_set_tabs_enabled(bool enabled_)
{
  for (int32_t i = 1; i < _main_widget.count(); ++i)
  {
    _main_widget.setTabEnabled(i, enabled_);
  }

  if (enabled_ == true)
  {
    _main_widget.setCurrentIndex(1);
  }
}

// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "tab-interface.hpp"

class TabSensorData : public TabInterface
{
  Q_OBJECT

public :
  TabSensorData(std::shared_ptr<PureCoolQt> pure_cool_qt_,
                QWidget                   * parent_ = nullptr);

protected :
  void _add_info(const std::string          & topic_,
                 QGridLayout                * layout_,
                 PureCoolQt::FunctionIntConst function_signal_);

private :
  TabSensorData            (const TabSensorData & other_) = delete;
  TabSensorData & operator=(const TabSensorData & other_) = delete;
};

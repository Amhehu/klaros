// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

#include "tab-sensor-data.hpp"
#include "label-value.hpp"

TabSensorData::TabSensorData(std::shared_ptr<PureCoolQt> pure_cool_qt_,
                             QWidget                   * parent_) :
  TabInterface(pure_cool_qt_, parent_)
{
  auto main_layout = new QGridLayout;
  setLayout(main_layout);

  _add_info(std::string("tact"), main_layout, &PureCoolQt::signal_tact);
  _add_info(std::string("hact"), main_layout, &PureCoolQt::signal_hact);
  _add_info(std::string("pm25"), main_layout, &PureCoolQt::signal_pm25);
  _add_info(std::string("pm10"), main_layout, &PureCoolQt::signal_pm10);
  _add_info(std::string("va10"), main_layout, &PureCoolQt::signal_va10);
  _add_info(std::string("noxl"), main_layout, &PureCoolQt::signal_noxl);

  auto button_update = new QPushButton(QString("update"), this);
  main_layout->addWidget(button_update, main_layout->rowCount(), 0, 1, 2);
  connect(button_update, &QPushButton::pressed, _pure_cool_qt.get(), &PureCoolQt::slot_update_sensor);
}

void TabSensorData::_add_info(const std::string          & topic_,
                              QGridLayout                * layout_,
                              PureCoolQt::FunctionIntConst function_signal_)
{
  auto row = layout_->rowCount();

  auto description = _pure_cool_qt->get_description(PureCoolController::INFO_TYPES::SENSOR, topic_);

  auto label_description = new QLabel    (QString::fromStdString(description), this);
  auto label_value       = new LabelValue(QString("unknown")                 , this);

  layout_->addWidget(label_description, row, 0);
  layout_->addWidget(label_value      , row, 1);

  connect(_pure_cool_qt.get(), function_signal_, label_value, qOverload<int32_t>(&LabelValue::set_value));
}

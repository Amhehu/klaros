// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

#include "label-value.hpp"
#include "slider-value.hpp"
#include "tab-device-state.hpp"

TabDeviceState::TabDeviceState(std::shared_ptr<PureCoolQt> pure_cool_qt_,
                               QWidget                   * parent_) :
  TabInterface(pure_cool_qt_, parent_)
{
  auto main_layout = new QGridLayout;
  setLayout(main_layout);

  _add_bool_entry(main_layout, std::string("fpwr"), &PureCoolQt::slot_fpwr_on, &PureCoolQt::slot_fpwr_off, &PureCoolQt::signal_fpwr);
  _add_bool_entry(main_layout, std::string("auto"), &PureCoolQt::slot_auto_on, &PureCoolQt::slot_auto_off, &PureCoolQt::signal_auto);
  _add_bool_entry(main_layout, std::string("fdir"), &PureCoolQt::slot_fdir_on, &PureCoolQt::slot_fdir_off, &PureCoolQt::signal_fdir);
  _add_bool_entry(main_layout, std::string("nmod"), &PureCoolQt::slot_nmod_on, &PureCoolQt::slot_nmod_off, &PureCoolQt::signal_nmod);

  _add_step_entry(main_layout, std::string("fnsp" ), QString("slower"), QString("higher"), &PureCoolQt::slot_fnsp_slower , &PureCoolQt::slot_fnsp_faster, &PureCoolQt::signal_fnsp );
  _add_step_entry(main_layout, std::string("range"), QString("closer"), QString("wider" ), &PureCoolQt::slot_range_closer, &PureCoolQt::slot_range_wider, &PureCoolQt::signal_range);

  std::vector<int32_t> valid_lower_values;
  valid_lower_values.reserve(350);
  for (int32_t i = 5; i <= 354; ++i)
  {
    valid_lower_values.push_back(i);
  }

  _add_slider_entry(main_layout, std::string("osal"), &PureCoolQt::slot_set_lower, &PureCoolQt::signal_lower, valid_lower_values);
}

void TabDeviceState::_add_bool_entry(QGridLayout                 * layout_,
                                     const std::string           & topic_,
                                     PureCoolQt::Function          function_slot_on_,
                                     PureCoolQt::Function          function_slot_off_,
                                     PureCoolQt::FunctionBoolConst function_signal_on_)
{
  auto description = QString::fromStdString(_pure_cool_qt->get_description(PureCoolController::INFO_TYPES::STATE, topic_));

  auto label_description = new QLabel     (description       , this);
  auto button_on         = new QPushButton(QString("on"     ), this);
  auto button_off        = new QPushButton(QString("off"    ), this);
  auto label_value       = new LabelValue (QString("unknown"), this);

  auto row = layout_->rowCount();

  layout_->addWidget(label_description, row, 0);
  layout_->addWidget(button_on        , row, 1);
  layout_->addWidget(button_off       , row, 2);
  layout_->addWidget(label_value      , row, 3);

  connect(button_on , &QPushButton::pressed, _pure_cool_qt.get(), function_slot_on_ );
  connect(button_off, &QPushButton::pressed, _pure_cool_qt.get(), function_slot_off_);

  connect(_pure_cool_qt.get(), function_signal_on_, label_value, qOverload<bool>(&LabelValue::set_value));
  connect(_pure_cool_qt.get(), function_signal_on_, button_on  , &QPushButton::setDisabled              );
  connect(_pure_cool_qt.get(), function_signal_on_, button_off , &QPushButton::setEnabled               );
}

void TabDeviceState::_add_step_entry(QGridLayout                * layout_,
                                     const std::string          & topic_,
                                     const QString              & label_lower_,
                                     const QString              & label_higher_,
                                     PureCoolQt::Function         function_slot_lower_,
                                     PureCoolQt::Function         function_slot_higher_,
                                     PureCoolQt::FunctionIntConst function_signal_value_)
{
  auto description = QString::fromStdString(_pure_cool_qt->get_description(PureCoolController::INFO_TYPES::STATE, topic_));

  auto label_description = new LabelValue (description       , this);
  auto button_lower      = new QPushButton(label_lower_      , this);
  auto button_higher     = new QPushButton(label_higher_     , this);
  auto label_value       = new LabelValue (QString("unknown"), this);

  auto row = layout_->rowCount();

  layout_->addWidget(label_description, row, 0);
  layout_->addWidget(button_lower     , row, 1);
  layout_->addWidget(button_higher    , row, 2);
  layout_->addWidget(label_value      , row, 3);

  connect(button_lower , &QPushButton::pressed, _pure_cool_qt.get(), function_slot_lower_ );
  connect(button_higher, &QPushButton::pressed, _pure_cool_qt.get(), function_slot_higher_);

  connect(_pure_cool_qt.get(), function_signal_value_, label_value, qOverload<int32_t>(&LabelValue::set_value));
}

void TabDeviceState::_add_slider_entry(QGridLayout                * layout_,
                                       const std::string          & topic_,
                                       PureCoolQt::FunctionInt      function_slot_value_,
                                       PureCoolQt::FunctionIntConst function_signal_value_,
                                       const std::vector<int32_t> & valid_values_)
{
  auto description = QString::fromStdString(_pure_cool_qt->get_description(PureCoolController::INFO_TYPES::STATE, topic_));

  auto label_description = new QLabel     (description                               , this);
  auto slider_value      = new SliderValue(valid_values_, Qt::Orientation::Horizontal, this);
  auto label_value       = new LabelValue (QString("unknown")                        , this);

  auto row = layout_->rowCount();

  layout_->addWidget(label_description, row, 0, 1, 1);
  layout_->addWidget(slider_value     , row, 1, 1, 2);
  layout_->addWidget(label_value      , row, 3, 1, 1);

  connect(slider_value       , &SliderValue::signal_value, _pure_cool_qt.get(), function_slot_value_                      );
  connect(_pure_cool_qt.get(), function_signal_value_    , slider_value       , &SliderValue::slot_value                  );
  connect(_pure_cool_qt.get(), function_signal_value_    , label_value        , qOverload<int32_t>(&LabelValue::set_value));
}

// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "slider-value.hpp"

SliderValue::SliderValue(const std::vector<int32_t> & valid_values_,
                         Qt::Orientation              orientation_,
                         QWidget                    * parent_) :
  QSlider(orientation_, parent_),
  _valid_values(valid_values_)
{
  setMinimum(0);
  setMaximum(std::max(0, int32_t(_valid_values.size()) - 1));
  setSingleStep(1);

  connect(this, &SliderValue::sliderReleased, this, &SliderValue::_step_changed);
}

void SliderValue::slot_value(int32_t value_)
{
  uint32_t position = 0;
  while ((position < _valid_values.size()) && (_valid_values[position] != value_))
  {
    ++position;
  }

  if (position < _valid_values.size())
  {
    setSliderPosition(position);
  }
}

void SliderValue::_step_changed()
{
  auto current_step = value();
  if ((0 <= current_step) && (current_step < int32_t(_valid_values.size())))
  {
    signal_value(_valid_values[current_step]);
  }
}

void SliderValue::keyPressEvent(QKeyEvent * event_)
{
  auto old_value = value();
  QSlider::keyPressEvent(event_);
  auto new_value = value();

  if (old_value != new_value)
  {
    _step_changed();
  }
}

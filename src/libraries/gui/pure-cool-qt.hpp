// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#pragma warning(push, 0)
#include <QObject>
#pragma warning(pop)

#include <controller/pure-cool-controller.hpp>

class PureCoolQt : public QObject, public PureCoolController
{
  Q_OBJECT

public :
  using Function          = void (PureCoolQt::*)();
  using FunctionBoolConst = void (PureCoolQt::*)(bool) const;
  using FunctionInt       = void (PureCoolQt::*)(int32_t);
  using FunctionIntConst  = void (PureCoolQt::*)(int32_t) const;
  using FunctionString    = void (PureCoolQt::*)(const QString &);

  PureCoolQt() = default;

signals :
  void signal_connection_possible      (bool state_) const;
  void signal_connection_status_changed(bool state_) const;

  void signal_fpwr(bool state_) const;
  void signal_auto(bool state_) const;
  void signal_oson(bool state_) const;
  void signal_nmod(bool state_) const;
  void signal_fdir(bool state_) const;

  void signal_fnsp(int32_t value_) const;
  void signal_cflr(int32_t value_) const;
  void signal_hflr(int32_t value_) const;
  void signal_sltm(int32_t value_) const;

  void signal_tact(int32_t value_) const;
  void signal_hact(int32_t value_) const;
  void signal_pm25(int32_t value_) const;
  void signal_pm10(int32_t value_) const;
  void signal_va10(int32_t value_) const;
  void signal_noxl(int32_t value_) const;

  void signal_lower(int32_t value_) const;
  void signal_range(int32_t value_) const;

public slots :
  void slot_set_client_id     (const QString & value_);
  void slot_set_password      (const QString & value_);
  void slot_set_server_address(const QString & value_);
  void slot_set_username      (const QString & value_);

  void slot_fpwr_on ();
  void slot_fpwr_off();

  void slot_auto_on ();
  void slot_auto_off();

  void slot_fdir_on ();
  void slot_fdir_off();

  void slot_nmod_on ();
  void slot_nmod_off();

  void slot_fnsp_slower();
  void slot_fnsp_faster();

  void slot_range_closer();
  void slot_range_wider ();

  void slot_set_lower(int32_t value_);
  void slot_set_range(int32_t value_);

  void slot_connect      ();
  void slot_update_sensor();

protected :
  virtual void on_success     (const mqtt::token & token_) override;
  virtual void connection_lost(const std::string & cause_) override;

  virtual void message_arrived(mqtt::const_message_ptr message_) override;

  void _signal_infos() const;

private :
  PureCoolQt            (const PureCoolQt & other_) = delete;
  PureCoolQt & operator=(const PureCoolQt & other_) = delete;

  void _test_connection_possible();
};

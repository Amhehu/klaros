// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "pure-cool-qt.hpp"

void PureCoolQt::on_success(const mqtt::token & token_)
{
  PureCoolController::on_success(token_);

  signal_connection_status_changed(true);
}

void PureCoolQt::connection_lost(const std::string & cause_)
{
  PureCoolController::connection_lost(cause_);

  signal_connection_status_changed(false);
}

void PureCoolQt::message_arrived(mqtt::const_message_ptr message_)
{
  PureCoolController::message_arrived(message_);

  _signal_infos();
}

void PureCoolQt::_signal_infos() const
{
  auto i_parser_state = _parser_map.find(INFO_TYPES::STATE);

  if (i_parser_state != _parser_map.end())
  {
    signal_fpwr(i_parser_state->second->get_infos_bool(std::string("fpwr")));
    signal_auto(i_parser_state->second->get_infos_bool(std::string("auto")));
    signal_oson(i_parser_state->second->get_infos_bool(std::string("oson")));
    signal_nmod(i_parser_state->second->get_infos_bool(std::string("nmod")));
    signal_fdir(i_parser_state->second->get_infos_bool(std::string("fdir")));

    signal_fnsp(i_parser_state->second->get_infos_int (std::string("fnsp")));
    signal_cflr(i_parser_state->second->get_infos_int (std::string("cflr")));
    signal_hflr(i_parser_state->second->get_infos_int (std::string("hflr")));
    signal_sltm(i_parser_state->second->get_infos_int (std::string("sltm")));

    auto upper = i_parser_state->second->get_infos_int(std::string("osau"));
    auto lower = i_parser_state->second->get_infos_int(std::string("osal"));
    auto range = upper - lower;
    signal_lower(lower);
    signal_range(range);
  }

  auto i_parser_sensor = _parser_map.find(INFO_TYPES::SENSOR);

  if (i_parser_sensor != _parser_map.end())
  {
    signal_tact(i_parser_sensor->second->get_infos_int(std::string("tact")));
    signal_hact(i_parser_sensor->second->get_infos_int(std::string("hact")));
    signal_pm25(i_parser_sensor->second->get_infos_int(std::string("pm25")));
    signal_pm10(i_parser_sensor->second->get_infos_int(std::string("pm10")));
    signal_va10(i_parser_sensor->second->get_infos_int(std::string("va10")));
    signal_noxl(i_parser_sensor->second->get_infos_int(std::string("noxl")));
  }
}

void PureCoolQt::_test_connection_possible()
{
  signal_connection_possible((_client_id                            .length() > 0) &&
                             (_server_address                       .length() > 0) &&
                             (_connection_options.get_user_name   ().length() > 0) &&
                             (_connection_options.get_password_str().length() > 0));
}

void PureCoolQt::slot_set_client_id(const QString & value_)
{
  set_client_id(value_.toStdString());
  _test_connection_possible();
}

void PureCoolQt::slot_set_password(const QString & value_)
{
  set_password(value_.toStdString());
  _test_connection_possible();
}

void PureCoolQt::slot_set_server_address(const QString & value_)
{
  set_server_address(value_.toStdString());
  _test_connection_possible();
}

void PureCoolQt::slot_set_username(const QString & value_)
{
  set_username(value_.toStdString());
  _test_connection_possible();
}

void PureCoolQt::slot_fpwr_on()
{
  _set_state_on(true);
}

void PureCoolQt::slot_fpwr_off()
{
  _set_state_on(false);
}

void PureCoolQt::slot_auto_on()
{
  _set_auto_on(true);
}

void PureCoolQt::slot_auto_off()
{
  _set_auto_on(false);
}

void PureCoolQt::slot_fdir_on()
{
  _set_forward_on(true);
}

void PureCoolQt::slot_fdir_off()
{
  _set_forward_on(false);
}

void PureCoolQt::slot_nmod_on()
{
  _set_nightmode_on(true);
}

void PureCoolQt::slot_nmod_off()
{
  _set_nightmode_on(false);
}

void PureCoolQt::slot_fnsp_slower()
{
  _set_fnsp_delta(-1);
}

void PureCoolQt::slot_fnsp_faster()
{
  _set_fnsp_delta(1);
}

void PureCoolQt::slot_range_closer()
{
  _set_range_delta(-1);
}

void PureCoolQt::slot_range_wider()
{
  _set_range_delta(1);
}

void PureCoolQt::slot_set_lower(int32_t value_)
{
  auto i_parser_state = _parser_map.find(INFO_TYPES::STATE);

  if (i_parser_state != _parser_map.end())
  {
    auto upper = i_parser_state->second->get_infos_int(std::string("osau"));
    auto lower = i_parser_state->second->get_infos_int(std::string("osal"));
    auto range = upper - lower;

    _set_range(value_, range);
  }
}

void PureCoolQt::slot_set_range(int32_t value_)
{
  auto i_parser_state = _parser_map.find(INFO_TYPES::STATE);

  if (i_parser_state != _parser_map.end())
  {
    auto lower = i_parser_state->second->get_infos_int(std::string("osal"));

    _set_range(lower, value_);
  }
}

void PureCoolQt::slot_connect()
{
  create_client   ();
  subscribe_status();
}

void PureCoolQt::slot_update_sensor()
{
  update_info(PureCoolController::INFO_TYPES::SENSOR);
}

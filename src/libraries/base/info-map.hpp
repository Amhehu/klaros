// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <map>
#include <string>

#include "info.hpp"

template<class InfoType>
class InfoMap : public std::map<std::string, InfoType>
{
public :
  InfoMap() = default;

  virtual InfoType get_info(const std::string & key_) const;

private :
  InfoMap            (const InfoMap & other_) = delete;
  InfoMap & operator=(const InfoMap & other_) = delete;
};

template<class InfoType>
inline InfoType InfoMap<InfoType>::get_info(const std::string & key_) const
{
  auto i_infos = this->find(key_);
  if (i_infos != this->end())
  {
    return i_infos->second;
  }
  else
  {
    return InfoType();
  }
}

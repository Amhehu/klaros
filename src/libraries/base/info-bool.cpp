// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "info-bool.hpp"

void InfoBool::parse(const std::string & value_)
{
  if (value_.compare(std::string("ON")) == 0)
  {
    _value = true;
  }
  else if (value_.compare(std::string("OFF")) == 0)
  {
    _value = false;
  }
}

std::string InfoBool::as_string() const
{
  if (_value == true)
  {
    return std::string("ON");
  }
  else
  {
    return std::string("OFF");
  }
}

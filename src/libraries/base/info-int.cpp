// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "info-int.hpp"

#include "common.hpp"

void InfoInt::parse(const std::string & value_)
{
  if (value_.compare(std::string("OFF")) == 0)
  {
    _value = -1;
  }
  else
  {
    _value = string_to_int(value_);
  }
}

std::string InfoInt::as_string() const
{
  if (_value < 0)
  {
    return std::string("OFF");
  }
  else
  {
    return std::to_string(_value);
  }
}

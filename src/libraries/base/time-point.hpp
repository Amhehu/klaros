// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>

class TimePoint
{
public:
  static TimePoint get_now();
  static std::string get_now_as_string();

  TimePoint         (                        ) = default;
  TimePoint         (const TimePoint & other_) = default;
  virtual ~TimePoint(                        ) = default;

  void set_to_now();

  uint32_t get_year       () const;
  uint32_t get_month      () const;
  uint32_t get_day        () const;
  uint32_t get_hour       () const;
  uint32_t get_minute     () const;
  uint32_t get_second     () const;
  uint32_t get_millisecond() const;

  void set_year       (uint32_t value_);
  void set_month      (uint32_t value_);
  void set_day        (uint32_t value_);
  void set_hour       (uint32_t value_);
  void set_minute     (uint32_t value_);
  void set_second     (uint32_t value_);
  void set_millisecond(uint32_t value_);

private:
  TimePoint & operator=(const TimePoint & other_) = delete;

  uint32_t _year        = 0;
  uint32_t _month       = 0;
  uint32_t _day         = 0;
  uint32_t _hour        = 0;
  uint32_t _minute      = 0;
  uint32_t _second      = 0;
  uint32_t _millisecond = 0;
};

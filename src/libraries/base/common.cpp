// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>

#include "common.hpp"

std::string leading_zero(uint32_t number_, uint32_t length_)
{
  auto as_string = std::to_string(number_);

  auto diff = int32_t(length_) - int32_t(as_string.length());
  if (diff > 0)
  {
    as_string.insert(0, diff, '0');
  }

  return as_string;
}

int32_t string_to_int(const std::string & string_, std::size_t * position_, int32_t base_)
{
  try
  {
    auto return_value = std::stoi(string_, position_, base_);
    return return_value;
  }
  catch (const std::invalid_argument & invalid_argument_)
  {
    std::cerr << "Invalid argument during conversion : " << invalid_argument_.what() << std::endl;
    return 0;
  }
  catch (const std::out_of_range & out_of_range_)
  {
    std::cerr << "Out of Range error during conversion : " << out_of_range_.what() << std::endl;
    return 0;
  }
  catch (const std::exception & exception_)
  {
    std::cerr << "Undefined error during conversion : " << exception_.what() << std::endl;
    return 0;
  }
}

std::vector<std::string> split(const std::string & string_, char seperator_)
{
  std::vector<std::string> result;

  if (string_.length() > 0)
  {
    std::size_t current = 0;
    std::size_t position = string_.find_first_of(seperator_, 0);

    while (position != std::string::npos)
    {
      result.emplace_back(string_, current, position - current);
      current = position + 1;
      position = string_.find_first_of(seperator_, current);
    }

    result.emplace_back(string_, current);
  }

  return result;
}

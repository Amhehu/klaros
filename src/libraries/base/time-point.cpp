// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <chrono>
#include <iomanip>
#include <iostream>

#include "common.hpp"
#include "time-point.hpp"

TimePoint TimePoint::get_now()
{
  TimePoint time_point;
  time_point.set_to_now();
  return time_point;
}

std::string TimePoint::get_now_as_string()
{
  auto now_as_time_point = TimePoint::get_now();

  return leading_zero(now_as_time_point.get_hour       (), 2) + std::string(":") +
         leading_zero(now_as_time_point.get_minute     (), 2) + std::string(":") +
         leading_zero(now_as_time_point.get_second     (), 2) + std::string(":") +
         leading_zero(now_as_time_point.get_millisecond(), 3);
}

void TimePoint::set_to_now()
{
  auto now_as_timepoint = std::chrono::system_clock::now();

  auto now_as_time_t = std::chrono::system_clock::to_time_t(now_as_timepoint);

  struct tm now_as_local_time;

  bool convert_okay = false;

#ifdef _MSC_VER
  convert_okay = localtime_s(&now_as_local_time, &now_as_time_t) == 0;
#else
  auto pointer = std::localtime(&now_as_time_t);

  if (pointer != nullptr)
  {
    convert_okay = true;
    now_as_local_time = *pointer;
  }
#endif

  if (convert_okay == true)
  {
    auto milliseconds = uint32_t(std::chrono::duration_cast<std::chrono::milliseconds>(now_as_timepoint - std::chrono::system_clock::from_time_t(std::mktime(&now_as_local_time))).count());

    set_year       (now_as_local_time.tm_year + 1900);
    set_month      (now_as_local_time.tm_mon  + 1   );
    set_day        (now_as_local_time.tm_mday       );
    set_hour       (now_as_local_time.tm_hour       );
    set_minute     (now_as_local_time.tm_min        );
    set_second     (now_as_local_time.tm_sec        );
    set_millisecond(milliseconds                    );
  }
}

uint32_t TimePoint::get_year() const
{
  return _year;
}

uint32_t TimePoint::get_month() const
{
  return _month;
}

uint32_t TimePoint::get_day() const
{
  return _day;
}

uint32_t TimePoint::get_hour() const
{
  return _hour;
}

uint32_t TimePoint::get_minute() const
{
  return _minute;
}

uint32_t TimePoint::get_second() const
{
  return _second;
}

uint32_t TimePoint::get_millisecond() const
{
  return _millisecond;
}

void TimePoint::set_year(uint32_t value_)
{
  _year = value_;
}

void TimePoint::set_month(uint32_t value_)
{
  _month = value_;
}

void TimePoint::set_day(uint32_t value_)
{
  _day = value_;
}

void TimePoint::set_hour(uint32_t value_)
{
  _hour = value_;
}

void TimePoint::set_minute(uint32_t value_)
{
  _minute = value_;
}

void TimePoint::set_second(uint32_t value_)
{
  _second = value_;
}

void TimePoint::set_millisecond(uint32_t value_)
{
  _millisecond = value_;
}

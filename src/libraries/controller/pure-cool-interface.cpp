// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <functional>
#include <iomanip>

#include <base/common.hpp>

#include "pure-cool-interface.hpp"

static const std::map<std::string, PureCoolController::INFO_TYPES> INFO_MAP =
{
  { std::string("faults"), PureCoolController::INFO_TYPES::FAULTS },
  { std::string("sensor"), PureCoolController::INFO_TYPES::SENSOR },
  { std::string("state" ), PureCoolController::INFO_TYPES::STATE  },
};

PureCoolInterface::PureCoolInterface() :
  PureCoolController()
{
  _function_list[std::string("sao")] = std::make_tuple(std::string("set_auto_on"        ), &PureCoolInterface::_call_set_auto_on                , std::string(" state[on|off]"                                               ));
  _function_list[std::string("sfo")] = std::make_tuple(std::string("set_forward_on"     ), &PureCoolInterface::_call_set_forward_on             , std::string(" state[on|off]"                                               ));
  _function_list[std::string("sno")] = std::make_tuple(std::string("set_nightmode_on"   ), &PureCoolInterface::_call_set_nightmode_on           , std::string(" state[on|off]"                                               ));
  _function_list[std::string("sr" )] = std::make_tuple(std::string("set_range"          ), &PureCoolInterface::_call_set_range                  , std::string(" lower[5..354] range[0,45,90,180,350] // lower + range <= 350"));
  _function_list[std::string("slt")] = std::make_tuple(std::string("set_sleep_time"     ), &PureCoolInterface::_call_set_sleep_time             , std::string(" duration[0..540]"                                            ));
  _function_list[std::string("ss" )] = std::make_tuple(std::string("set_speed"          ), &PureCoolInterface::_call_set_speed                  , std::string(" speed[1..10]"                                                ));
  _function_list[std::string("sso")] = std::make_tuple(std::string("set_state_on"       ), &PureCoolInterface::_call_set_state_on               , std::string(" state[on|off]"                                               ));
  _function_list[std::string("d"  )] = std::make_tuple(std::string("decrease"           ), &PureCoolInterface::_call_decrease_speed             , std::string(""                                                             ));
  _function_list[std::string("i"  )] = std::make_tuple(std::string("increase"           ), &PureCoolInterface::_call_increase_speed             , std::string(""                                                             ));
  _function_list[std::string("ts" )] = std::make_tuple(std::string("toggle_state"       ), &PureCoolInterface::_call_toggle_state               , std::string(""                                                             ));
  _function_list[std::string("ui" )] = std::make_tuple(std::string("update_info"        ), &PureCoolInterface::_call_update_info                , std::string(" infotype[state|sensor]"                                      ));
  _function_list[std::string("ods")] = std::make_tuple(std::string("output_device_state"), &PureCoolInterface::_call_output_current_device_state, std::string(" <infoname>[..]"                                              ));
  _function_list[std::string("osd")] = std::make_tuple(std::string("output_sensor_data" ), &PureCoolInterface::_call_output_current_sensor_data , std::string(" <infoname>[..]"                                              ));
}

void PureCoolInterface::command(const std::string & command_)
{
  if ((command_.compare(std::string("help")) != 0) && (command_.compare(std::string("h")) != 0))
  {
    auto result = split(command_, ' ');

    if (0 < result.size())
    {
      auto function_iterator = _function_list.find(result[0]);

      if (function_iterator != _function_list.end())
      {
        std::invoke(std::get<1>(function_iterator->second), *this, result);
      }
    }
  }
  else
  {
    show_help();
  }
}

void PureCoolInterface::show_help()
{
  std::cout << "available commands :" << std::endl << std::endl;

  for (const auto & i_function_list : _function_list)
  {
    std::cout << std::setw(5) << i_function_list.first << std::setw(20) << std::get<0>(i_function_list.second) << std::get<2>(i_function_list.second) << std::endl;
  }

  std::cout << std::endl;
}

void PureCoolInterface::_call_set_auto_on(const std::vector<std::string> & command_)
{
  _call_bool_function(&PureCoolInterface::_set_auto_on, command_);
}

void PureCoolInterface::_call_set_forward_on(const std::vector<std::string> & command_)
{
  _call_bool_function(&PureCoolInterface::_set_forward_on, command_);
}

void PureCoolInterface::_call_set_nightmode_on(const std::vector<std::string> & command_)
{
  _call_bool_function(&PureCoolInterface::_set_nightmode_on, command_);
}

void PureCoolInterface::_call_set_range(const std::vector<std::string> & command_)
{
  if (command_.size() == 3)
  {
    auto lower = string_to_int(command_[1]);
    auto range = string_to_int(command_[2]);
    _set_range(lower, range);
  }
}

void PureCoolInterface::_call_set_sleep_time(const std::vector<std::string> & command_)
{
  _call_int_function(&PureCoolInterface::_set_sleep_time, command_);
}

void PureCoolInterface::_call_set_speed(const std::vector<std::string> & command_)
{
  _call_int_function(&PureCoolInterface::_set_speed, command_);
}

void PureCoolInterface::_call_set_state_on(const std::vector<std::string> & command_)
{
  _call_bool_function(&PureCoolInterface::_set_state_on, command_);
}

void PureCoolInterface::_call_bool_function(PureCoolControllerFunctionBool pure_cool_controller_function_bool_, const std::vector<std::string> & command_)
{
  if (command_.size() == 2)
  {
    if (command_[1].compare("on") == 0)
    {
      std::invoke(pure_cool_controller_function_bool_, *this, true);
    }
    else if (command_[1].compare("off") == 0)
    {
      std::invoke(pure_cool_controller_function_bool_, *this, false);
    }
  }
}

void PureCoolInterface::_call_int_function(PureCoolControllerFunctionInt pure_cool_controller_function_int_, const std::vector<std::string> & command_)
{
  if (command_.size() == 2)
  {
    auto value = string_to_int(command_[1]);
    std::invoke(pure_cool_controller_function_int_, *this, value);
  }
}

void PureCoolInterface::_call_decrease_speed(const std::vector<std::string> & command_)
{
  if (command_.size() == 1)
  {
    _set_fnsp_delta(-1);
  }
}

void PureCoolInterface::_call_increase_speed(const std::vector<std::string> & command_)
{
  if (command_.size() == 1)
  {
    _set_fnsp_delta(1);
  }
}

void PureCoolInterface::_call_toggle_state(const std::vector<std::string> & command_)
{
  if (command_.size() == 1)
  {
    _toggle_state();
  }
}

void PureCoolInterface::_call_update_info(const std::vector<std::string> & command_)
{
  if (command_.size() == 2)
  {
    auto i_info_map = INFO_MAP.find(command_[1]);

    if (i_info_map != INFO_MAP.end())
    {
      update_info(i_info_map->second);
    }
  }
}

void PureCoolInterface::_call_output_current_device_state(const std::vector<std::string> & command_)
{
  _output_infos(INFO_TYPES::STATE, command_);
}

void PureCoolInterface::_call_output_current_sensor_data(const std::vector<std::string> & command_)
{
  _output_infos(INFO_TYPES::SENSOR, command_);
}

void PureCoolInterface::_output_infos(INFO_TYPES info_type_, const std::vector<std::string> & command_)
{
  if (command_.size() == 1)
  {
    _parser_map[info_type_]->output_all_infos();
  }
  else if (command_.size() == 2)
  {
    _parser_map[info_type_]->output_info(command_[1]);
  }
}

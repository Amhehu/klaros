// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <string>

#include <base/time-point.hpp>

#include "message-builder.hpp"

MessageBuilder::MessageBuilder() :
  _string_buffer(),
  _writer(_string_buffer)
{
}

std::string MessageBuilder::get_string() const
{
  return std::string(_string_buffer.GetString());
}

void MessageBuilder::start_object(const std::string & key_)
{
  if (key_.length() > 0)
  {
    _writer.Key(key_.c_str());
  }

  _writer.StartObject();
}

void MessageBuilder::end_object()
{
  _writer.EndObject();
}

void MessageBuilder::add_time(const std::string & time_)
{
  auto time = time_;

  if (time.length() == 0)
  {
    auto now = TimePoint::get_now();

    time = std::to_string(now.get_year  ()) + std::string("-") +
           std::to_string(now.get_month ()) + std::string("-") +
           std::to_string(now.get_day   ()) + std::string("T") +
           std::to_string(now.get_hour  ()) + std::string(":") +
           std::to_string(now.get_minute()) + std::string(":") +
           std::to_string(now.get_second()) + std::string("Z");
  }

  add_field(std::string("time"), time);
}

void MessageBuilder::add_mode_reason(const std::string & mode_reason_)
{
  add_field(std::string("mode-reason"), (mode_reason_.length() == 0) ? std::string("STATE-LAPP") : mode_reason_);
}

void MessageBuilder::add_message(const std::string & message_)
{
  add_field(std::string("msg"), message_);
}

void MessageBuilder::add_field(const std::string & key_, const std::string & content_)
{
  _writer.Key   (key_    .c_str());
  _writer.String(content_.c_str());
}

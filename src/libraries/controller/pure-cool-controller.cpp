// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <base/common.hpp>

#include "message-builder.hpp"
#include "pure-cool-controller.hpp"

static const std::map<PureCoolController::INFO_TYPES, std::string> INFO_NAMES =
{
  { PureCoolController::INFO_TYPES::FAULTS, std::string("REQUEST-CURRENT-FAULTS"                         ) },
  { PureCoolController::INFO_TYPES::SENSOR, std::string("REQUEST-PRODUCT-ENVIRONMENT-CURRENT-SENSOR-DATA") },
  { PureCoolController::INFO_TYPES::STATE , std::string("REQUEST-CURRENT-STATE"                          ) },
};

static const std::vector<int32_t> VALID_RANGE_VALUES =
{
  {  0},
  { 45},
  { 90},
  {180},
  {350},
};

void PureCoolController::subscribe_status()
{
  subscribe       (std::string("438/") + _connection_options.get_user_name() + std::string("/status/current"));
  _command_topic = std::string("438/") + _connection_options.get_user_name() + std::string("/command"       );
}

void PureCoolController::update_info(INFO_TYPES info_type_)
{
  auto i_info_name = INFO_NAMES.find(info_type_);

  if (i_info_name != INFO_NAMES.end())
  {
    MessageBuilder message_builder;

    message_builder.start_object();

    message_builder.add_time       ();
    message_builder.add_mode_reason();
    message_builder.add_message    (i_info_name->second);

    message_builder.end_object();

    _send_message(_command_topic, message_builder.get_string());
  }
}

std::string PureCoolController::get_description(INFO_TYPES          info_type_,
                                                const std::string & key_) const
{
  auto i_parser = _parser_map.find(info_type_);
  if (i_parser != _parser_map.end())
  {
    return i_parser->second->get_info_description(key_);
  }
  else
  {
    return key_;
  }
}

void PureCoolController::_set_auto_on(bool state_)
{
  _send_data_bool_message(std::string("auto"), state_);
}

void PureCoolController::_set_forward_on(bool state_)
{
  _send_data_bool_message(std::string("fdir"), state_);
}

void PureCoolController::_set_nightmode_on(bool state_)
{
  _send_data_bool_message(std::string("nmod"), state_);
}

void PureCoolController::_set_range(int32_t lower_, int32_t range_)
{
  if ((5 <= lower_) && (lower_ <= 354) && (0 <= range_) && (range_ <= 350))
  {
    auto upper = lower_ + range_;
    if (upper > 350)
    {
      upper = 350;
      lower_ = upper - range_;
    }

    MessageBuilder message_builder;

    message_builder.start_object();

    message_builder.add_time       ();
    message_builder.add_mode_reason();
    message_builder.add_message    (std::string("STATE-SET"));

    message_builder.start_object(std::string("data"));
    message_builder.add_field(std::string("osal"), leading_zero(lower_, 4));
    message_builder.add_field(std::string("osau"), leading_zero(upper , 4));
    message_builder.add_field(std::string("oson"), std::string("ON"  )    );
    message_builder.add_field(std::string("fpwr"), std::string("ON"  )    );
    message_builder.add_field(std::string("ancp"), std::string("CUST")    );
    message_builder.end_object();

    message_builder.end_object();

    _send_message(_command_topic, message_builder.get_string());
  }
}

void PureCoolController::_set_sleep_time(int32_t value_)
{
  _send_data_int_message(std::string("sltm"), value_);
}

void PureCoolController::_set_speed(int32_t value_)
{
  _send_data_int_message(std::string("fnsp"), value_);
}

void PureCoolController::_set_state_on(bool state_)
{
  _send_data_bool_message(std::string("fpwr"), state_);
}

void PureCoolController::_set_fnsp_delta(int32_t delta_speed_)
{
  auto as_int = _parser_map[INFO_TYPES::STATE]->get_infos_int(std::string("fnsp"));

  auto result = std::max(1, std::min(10, as_int + delta_speed_));

  if (as_int != result)
  {
    _set_speed(result);
  }
}

void PureCoolController::_set_range_delta(int32_t delta_range_)
{
  if (delta_range_ != 0)
  {
    auto as_int_osal = _parser_map[INFO_TYPES::STATE]->get_infos_int(std::string("osal"));
    auto as_int_osau = _parser_map[INFO_TYPES::STATE]->get_infos_int(std::string("osau"));

    auto current_range = as_int_osau - as_int_osal;

    auto i_range = std::find(VALID_RANGE_VALUES.begin(), VALID_RANGE_VALUES.end(), current_range);

    auto position = int32_t(std::distance(VALID_RANGE_VALUES.begin(), i_range));

    auto new_position = std::min(std::max(position + delta_range_, 0), int32_t(VALID_RANGE_VALUES.size()) - 1);

    _set_range(as_int_osal, VALID_RANGE_VALUES[new_position]);
  }
}

void PureCoolController::_send_data_bool_message(const std::string & field_, bool value_)
{
  _send_data_message(field_, value_ == true ? std::string("ON") : std::string("OFF"));
}

void PureCoolController::_send_data_int_message(const std::string & field_, int32_t value_)
{
  _send_data_message(field_, leading_zero(value_, 4));
}

void PureCoolController::_send_data_message(const std::string & field_, const std::string & value_)
{
  MessageBuilder message_builder;

  message_builder.start_object();

  message_builder.add_time       ();
  message_builder.add_mode_reason();
  message_builder.add_message    (std::string("STATE-SET"));

  message_builder.start_object(std::string("data"));
  message_builder.add_field   (field_, value_);
  message_builder.end_object  ();

  message_builder.end_object();

  _send_message(_command_topic, message_builder.get_string());
}

void PureCoolController::_toggle_state()
{
  _set_state_on(_parser_map[INFO_TYPES::STATE]->get_infos_bool(std::string("fpwr")) == false);
}

void PureCoolController::on_success(const mqtt::token & token_)
{
  MessageCallback::on_success(token_);

  update_info(PureCoolController::INFO_TYPES::STATE);
}

void PureCoolController::message_arrived(mqtt::const_message_ptr message_)
{
  if (message_ != nullptr)
  {
    rapidjson::Document document;
    document.Parse(message_->get_payload().c_str());

    if (document.IsObject() == true)
    {
      for (const auto & i_parser_map : _parser_map)
      {
        std::invoke(&DataContainer::parse_document, i_parser_map.second, document);
      }
    }
  }
}

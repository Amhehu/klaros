// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "sensor-data.hpp"

SensorData::SensorData()
{
  _fill_maps();
}

void SensorData::parse_document(const rapidjson::Document & document_)
{
  if (document_.IsObject() == true)
  {
    if ((document_.HasMember("msg") == true) && (document_["msg"].IsString() == true))
    {
      if (std::string("ENVIRONMENTAL-CURRENT-SENSOR-DATA").compare(document_["msg"].GetString()) == 0)
      {
        if ((document_.HasMember("data") == true) && (document_["data"].IsObject() == true))
        {
          for (auto i_member = document_["data"].MemberBegin(); i_member != document_["data"].MemberEnd(); ++i_member)
          {
            if ((i_member->name.IsString() == true) && (i_member->value.IsString() == true))
            {
              assign_message(i_member->name.GetString(), i_member->value.GetString());
            }
          }
        }
      }
    }
  }
}

void SensorData::_fill_maps()
{
  _add_info(std::string("tact"), std::make_pair(std::string("temperature")           , DataContainer::DATA_TYPES::INT));
  _add_info(std::string("hact"), std::make_pair(std::string("humidity")              , DataContainer::DATA_TYPES::INT));
  _add_info(std::string("pm25"), std::make_pair(std::string("particulare matter 2.5"), DataContainer::DATA_TYPES::INT));
  _add_info(std::string("pm10"), std::make_pair(std::string("particulare matter 10") , DataContainer::DATA_TYPES::INT));
  _add_info(std::string("va10"), std::make_pair(std::string("volatile organic")      , DataContainer::DATA_TYPES::INT));
  _add_info(std::string("noxl"), std::make_pair(std::string("nitrogen dioxide")      , DataContainer::DATA_TYPES::INT));
}

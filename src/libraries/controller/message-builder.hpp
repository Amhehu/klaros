// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#pragma warning(push, 0)
#include <rapidjson/writer.h>
#pragma warning(pop)

class MessageBuilder
{
public :
  MessageBuilder();

  std::string get_string() const;

  void start_object(const std::string & key_ = std::string());
  void end_object ();

  void add_time       (const std::string & time_        = std::string());
  void add_mode_reason(const std::string & mode_reason_ = std::string());
  void add_message    (const std::string & message_                    );

  void add_field(const std::string & key_, const std::string & content_);

private :
  MessageBuilder            (const MessageBuilder & other_) = delete;
  MessageBuilder & operator=(const MessageBuilder & other_) = delete;

  rapidjson::StringBuffer                    _string_buffer;
  rapidjson::Writer<rapidjson::StringBuffer> _writer;
};

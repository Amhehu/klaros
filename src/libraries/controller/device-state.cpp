// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <functional>
#include <iostream>

#include <base/common.hpp>

#include "device-state.hpp"

DeviceState::DeviceState()
{
  _fill_maps();
}

void DeviceState::parse_document(const rapidjson::Document & document_)
{
  if (document_.IsObject() == true)
  {
    if ((document_.HasMember("msg") == true) && (document_["msg"].IsString() == true))
    {
      auto i_map_device_state_msg = _map_device_state_msg.find(document_["msg"].GetString());
      if (i_map_device_state_msg != _map_device_state_msg.end())
      {
        std::invoke(i_map_device_state_msg->second, *this, document_);
      }
    }
  }
}

void DeviceState::_fill_maps()
{
  _add_info(std::string("fpwr"), std::make_pair(std::string("fan power"              ), DataContainer::DATA_TYPES::BOOL  ));
  _add_info(std::string("auto"), std::make_pair(std::string("auto mode"              ), DataContainer::DATA_TYPES::BOOL  ));
  _add_info(std::string("oson"), std::make_pair(std::string("oscillating"            ), DataContainer::DATA_TYPES::BOOL  ));
  _add_info(std::string("nmod"), std::make_pair(std::string("night mode"             ), DataContainer::DATA_TYPES::BOOL  ));
  _add_info(std::string("fdir"), std::make_pair(std::string("forward direction"      ), DataContainer::DATA_TYPES::BOOL  ));
  _add_info(std::string("fnsp"), std::make_pair(std::string("fan speed"              ), DataContainer::DATA_TYPES::INT   ));
  _add_info(std::string("osal"), std::make_pair(std::string("oscilate angle lower"   ), DataContainer::DATA_TYPES::INT   ));
  _add_info(std::string("osau"), std::make_pair(std::string("oscilate angle upper"   ), DataContainer::DATA_TYPES::INT   ));
  _add_info(std::string("cflr"), std::make_pair(std::string("filter remaining carbon"), DataContainer::DATA_TYPES::INT   ));
  _add_info(std::string("hflr"), std::make_pair(std::string("filter remaining hepa"  ), DataContainer::DATA_TYPES::INT   ));
  _add_info(std::string("sltm"), std::make_pair(std::string("sleep time"             ), DataContainer::DATA_TYPES::INT   ));
  _add_info(std::string("cflt"), std::make_pair(std::string("filter type carbon"     ), DataContainer::DATA_TYPES::STRING));
  _add_info(std::string("hflt"), std::make_pair(std::string("filter type hepa"       ), DataContainer::DATA_TYPES::STRING));

  _map_device_state_msg[std::string("CURRENT-STATE")] = &DeviceState::_parse_current_state;
  _map_device_state_msg[std::string("STATE-CHANGE" )] = &DeviceState::_parse_state_change ;
}

void DeviceState::_parse_state_change(const rapidjson::Document & document_)
{
  if ((document_.HasMember("product-state") == true) && (document_["product-state"].IsObject() == true))
  {
    for (auto i_member = document_["product-state"].MemberBegin(); i_member != document_["product-state"].MemberEnd(); ++i_member)
    {
      if (i_member->value.IsArray() == true)
      {
        auto as_array = i_member->value.GetArray();

        if (as_array.Size() == 2)
        {
          if (as_array[1].IsString() == true)
          {
            assign_message(i_member->name.GetString(), as_array[1].GetString());
          }
        }
      }
    }
  }
}

void DeviceState::_parse_current_state(const rapidjson::Document & document_)
{
  if ((document_.HasMember("product-state") == true) && (document_["product-state"].IsObject() == true))
  {
    for (auto i_member = document_["product-state"].MemberBegin(); i_member != document_["product-state"].MemberEnd(); ++i_member)
    {
      if ((i_member->name.IsString() == true) && (i_member->value.IsString() == true))
      {
        assign_message(i_member->name.GetString(), i_member->value.GetString());
      }
    }
  }
}

// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <map>

#include "pure-cool-controller.hpp"

class PureCoolInterface : public PureCoolController
{
public :
  using PureCoolInterfaceFunction = void (PureCoolInterface::*)(const std::vector<std::string> &);

  PureCoolInterface();

  void command(const std::string & command_);

  void show_help();

protected :
  void _call_set_auto_on     (const std::vector<std::string> & command_);
  void _call_set_forward_on  (const std::vector<std::string> & command_);
  void _call_set_nightmode_on(const std::vector<std::string> & command_);
  void _call_set_range       (const std::vector<std::string> & command_);
  void _call_set_sleep_time  (const std::vector<std::string> & command_);
  void _call_set_speed       (const std::vector<std::string> & command_);
  void _call_set_state_on    (const std::vector<std::string> & command_);

  void _call_bool_function(PureCoolControllerFunctionBool pure_cool_controller_function_bool_, const std::vector<std::string> & command_);
  void _call_int_function (PureCoolControllerFunctionInt  pure_cool_controller_function_int_ , const std::vector<std::string> & command_);

  void _call_decrease_speed(const std::vector<std::string> & command_);
  void _call_increase_speed(const std::vector<std::string> & command_);

  void _call_toggle_state(const std::vector<std::string> & command_);

  void _call_update_info(const std::vector<std::string> & command_);

  void _call_output_current_device_state(const std::vector<std::string> & command_);
  void _call_output_current_sensor_data (const std::vector<std::string> & command_);

  void _output_infos(INFO_TYPES info_type_, const std::vector<std::string> & command_);

  std::map<std::string, std::tuple<std::string, PureCoolInterfaceFunction, std::string> > _function_list;

private :
  PureCoolInterface            (const PureCoolInterface & other_) = delete;
  PureCoolInterface & operator=(const PureCoolInterface & other_) = delete;
};

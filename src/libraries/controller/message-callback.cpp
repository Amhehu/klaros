// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "message-callback.hpp"

static const uint32_t MAX_NUMBER_RETRIES = 5;

MessageCallback::~MessageCallback()
{
  _closing = true;

  if (_async_client != nullptr)
  {
    unsubscribe_all();

    _async_client->disconnect();

    _async_client.reset();
  }
}

void MessageCallback::create_client()
{
  _connection_options.set_keep_alive_interval(200);
  _connection_options.set_clean_session(true);

  auto complete_address = _server_address + std::string(":") + std::to_string(_port);
  _async_client = std::make_unique<mqtt::async_client>(complete_address, _client_id);

  if (_async_client != nullptr)
  {
    _async_client->set_callback(*this);
    _async_client->connect(_connection_options, nullptr, *this)->wait_for(std::chrono::milliseconds(2'000));
  }
}

void MessageCallback::subscribe(const std::string & topic_, int32_t qos_)
{
  if (_async_client != nullptr)
  {
    _subscriptions.insert(topic_);

    try
    {
      _async_client->subscribe(topic_, qos_, nullptr, *this);
      _subscriptions.insert(topic_);
    }
    catch (const mqtt::exception & exception_)
    {
      std::cerr << "error during subscription : " << exception_.what() << std::endl;
    }
  }
}

void MessageCallback::unsubscribe_all()
{
  for (const auto & i_subscriptions : _subscriptions)
  {
    _async_client->unsubscribe(i_subscriptions, nullptr, *this);
  }
}

const std::string & MessageCallback::get_client_id() const
{
  return _client_id;
}

const std::string & MessageCallback::get_server_address() const
{
  return _server_address;
}

const mqtt::connect_options & MessageCallback::get_connection_options() const
{
  return _connection_options;
}

void MessageCallback::set_client_id(const std::string & value_)
{
  _client_id = value_;
}

void MessageCallback::set_server_address(const std::string & value_)
{
  _server_address = value_;
}

void MessageCallback::set_password(const std::string & value_)
{
  _connection_options.set_password(value_);
}

void MessageCallback::set_username(const std::string & value_)
{
  _connection_options.set_user_name(value_);
}

void MessageCallback::_reconnect()
{
  std::this_thread::sleep_for(std::chrono::milliseconds(2'000));

  if (_async_client != nullptr)
  {
    try
    {
      _async_client->connect(_connection_options, nullptr, *this);
    }
    catch (const mqtt::exception & exception_)
    {
      std::cerr << "error during reconnect : " << exception_.what() << std::endl;
    }
  }
}

void MessageCallback::on_failure(const mqtt::token &)
{
  if (_closing == false)
  {
    std::cout << "connection attempt failed" << std::endl;

    if (_number_retry < MAX_NUMBER_RETRIES)
    {
      ++_number_retry;
      _reconnect();
    }
    else
    {
      std::cout << "after " << _number_retry << " tries stopped to reconnect" << std::endl;
    }
  }
}

void MessageCallback::on_success(const mqtt::token &)
{
  if (_connection_established == false)
  {
    _connection_established = true;
    std::cout << "connection established" << std::endl;
  }
}

void MessageCallback::_send_message(const std::string & topic_, const std::string & payload_, std::chrono::milliseconds timeout_)
{
  if (_async_client != nullptr)
  {
    auto pub_message = mqtt::make_message(topic_, payload_);
    pub_message->set_qos(1);

    try
    {
      _async_client->publish(pub_message)->wait_for(timeout_);
    }
    catch (const mqtt::exception & exception_)
    {
      std::cerr << "error during sending message : " << exception_.what() << std::endl;
    }
  }
}

void MessageCallback::connected(const std::string &)
{
  auto local_copy = _subscriptions;

  for (const auto & i_subscriptions : local_copy)
  {
    subscribe(i_subscriptions);
  }
}

void MessageCallback::connection_lost(const std::string & cause_)
{
  _connection_established = false;

  std::cout << std::endl << "connection lost";
  if (cause_.empty() == false)
  {
    std::cout << " cause : " << cause_;
  }
  std::cout << std::endl;

  std::cout << "reconnecting..." << std::endl;

  _number_retry = 0;
  _reconnect();
}

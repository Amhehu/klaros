// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <iomanip>

#include <base/common.hpp>

#include "data-container.hpp"

std::string DataContainer::get_info_description(const std::string & key_) const
{
  auto i_info_table = _info_table.find(key_);
  if (i_info_table != _info_table.end())
  {
    return i_info_table->second.first;
  }
  else
  {
    return key_;
  }
}

void DataContainer::_add_info(const std::string & key_, const std::pair<std::string, DATA_TYPES> & value_)
{
  _max_length_description = std::max(_max_length_description, uint32_t(value_.first.length()));

  _info_table[key_] = value_;
}

void DataContainer::assign_message(const std::string & key_, const std::string & value_)
{
  auto i_info = _info_table.find(key_);
  if (i_info != _info_table.end())
  {
    switch (i_info->second.second)
    {
      case DATA_TYPES::BOOL   : _infos_map_bool  [key_].parse(value_); break;
      case DATA_TYPES::INT    : _infos_map_int   [key_].parse(value_); break;
      case DATA_TYPES::STRING : _infos_map_string[key_].parse(value_); break;
    }
  }
}

bool DataContainer::get_infos_bool(const std::string & key_) const
{
  return _infos_map_bool.get_info(key_).get_value();
}

int32_t DataContainer::get_infos_int(const std::string & key_) const
{
  return _infos_map_int.get_info(key_).get_value();
}

std::string DataContainer::get_infos_string(const std::string & key_) const
{
  return _infos_map_string.get_info(key_).get_value();
}

void DataContainer::output_all_infos()
{
  std::cout << "current state :" << std::endl;

  for (const auto & i_infos_map_bool : _infos_map_bool)
  {
    std::cout << std::setw(_max_length_description) << std::string(get_info_description(i_infos_map_bool.first)) << " : " << i_infos_map_bool.second.as_string() << std::endl;
  }

  for (const auto & i_infos_map_int : _infos_map_int)
  {
    std::cout << std::setw(_max_length_description) << std::string(get_info_description(i_infos_map_int.first)) << " : " << i_infos_map_int.second.as_string() << std::endl;
  }

  for (const auto & i_infos_map_string : _infos_map_string)
  {
    std::cout << std::setw(_max_length_description) << std::string(get_info_description(i_infos_map_string.first)) << " : " << i_infos_map_string.second.as_string() << std::endl;
  }

  std::cout << std::endl;
}

void DataContainer::output_info(const std::string & key_)
{
  bool found = false;

  for (const auto & i_infos_map_bool : _infos_map_bool)
  {
    if (key_.compare(i_infos_map_bool.first) == 0)
    {
      std::cout << std::string(get_info_description(i_infos_map_bool.first)) << " : " << i_infos_map_bool.second.as_string() << std::endl;
      found = true;
    }
  }

  if (found == false)
  {
    for (const auto & i_infos_map_int : _infos_map_int)
    {
      if (key_.compare(i_infos_map_int.first) == 0)
      {
        std::cout << std::string(get_info_description(i_infos_map_int.first)) << " : " << i_infos_map_int.second.as_string() << std::endl;
      }
    }
  }

  if (found == false)
  {
    for (const auto & i_infos_map_string : _infos_map_string)
    {
      if (key_.compare(i_infos_map_string.first) == 0)
      {
        std::cout << std::string(get_info_description(i_infos_map_string.first)) << " : " << i_infos_map_string.second.as_string() << std::endl;
      }
    }
  }
}

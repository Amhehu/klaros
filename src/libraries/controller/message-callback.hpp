// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <memory>
#include <unordered_set>

#pragma warning(push, 0)
#include <mqtt/async_client.h>
#pragma warning(pop)

class MessageCallback : public virtual mqtt::callback, public virtual mqtt::iaction_listener
{
public :
  MessageCallback() = default;
  virtual ~MessageCallback();

  void create_client();
  void subscribe(const std::string & topic_, int32_t qos_ = 1);
  void unsubscribe_all();

  const std::string           & get_client_id         () const;
  const std::string           & get_server_address    () const;
  const mqtt::connect_options & get_connection_options() const;

  virtual void set_client_id     (const std::string & value_);
  virtual void set_password      (const std::string & value_);
  virtual void set_server_address(const std::string & value_);
  virtual void set_username      (const std::string & value_);

protected :
  bool _connection_established = false;
  mqtt::connect_options _connection_options;

  std::string _client_id;
  std::string _server_address;

  void _send_message(const std::string       & topic_,
                     const std::string       & payload_,
                     std::chrono::milliseconds timeout_ = std::chrono::milliseconds(2'000));

  virtual void on_failure(const mqtt::token & token_) override;
  virtual void on_success(const mqtt::token & token_) override;

  virtual void connected      (const std::string & cause_) override;
  virtual void connection_lost(const std::string & cause_) override;

private :
  MessageCallback            (const MessageCallback & other_) = delete;
  MessageCallback & operator=(const MessageCallback & other_) = delete;

  void _reconnect();

  bool     _closing      = false;
  uint32_t _port         = 1883;
  uint32_t _number_retry = 0;

  std::unique_ptr<mqtt::async_client> _async_client;

  std::unordered_set<std::string> _subscriptions;
};

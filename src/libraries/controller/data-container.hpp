// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>
#include <map>
#include <string>

#pragma warning(push, 0)
#include <rapidjson/document.h>
#pragma warning(pop)

#include <base/info-map.hpp>
#include <base/info-bool.hpp>
#include <base/info-int.hpp>
#include <base/info-string.hpp>

class DataContainer
{
public :
  enum class DATA_TYPES
  {
    BOOL = 0,
    INT,
    STRING,
  };

  using FunctionParse = void (DataContainer::*)(const std::string & key_, const std::string & info_);

  DataContainer() = default;

  virtual void parse_document(const rapidjson::Document & document_) = 0;

  void assign_message(const std::string & key_, const std::string & value_);

  bool        get_infos_bool  (const std::string & key_) const;
  int32_t     get_infos_int   (const std::string & key_) const;
  std::string get_infos_string(const std::string & key_) const;

  void output_all_infos();
  void output_info(const std::string & key_);

  std::string get_info_description(const std::string & key_) const;

protected :
  virtual void _fill_maps() = 0;

  void _add_info(const std::string & key_, const std::pair<std::string, DATA_TYPES> & value_);

  InfoMap<InfoBool  > _infos_map_bool  ;
  InfoMap<InfoInt   > _infos_map_int   ;
  InfoMap<InfoString> _infos_map_string;

private :
  DataContainer            (const DataContainer & other_) = delete;
  DataContainer & operator=(const DataContainer & other_) = delete;

  // key_word -> <description, type>
  std::map<std::string, std::pair<std::string, DATA_TYPES> > _info_table;

  uint32_t _max_length_description = 0;
};

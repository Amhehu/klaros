// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "data-container.hpp"

class DeviceState : public DataContainer
{
public :
  using FunctionMsg = void (DeviceState::*)(const rapidjson::Document & document_);

  DeviceState();

  virtual void parse_document(const rapidjson::Document & document_) override;

protected :
  virtual void _fill_maps() override;

  std::map<std::string, FunctionMsg> _map_device_state_msg;

private :
  DeviceState            (const DeviceState & other_) = delete;
  DeviceState & operator=(const DeviceState & other_) = delete;

  void _parse_current_state(const rapidjson::Document & document_);
  void _parse_state_change (const rapidjson::Document & document_);
};

// Klaros
// Copyright (C) 2020 Silvio Tristram
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <map>
#include <memory>

#include "device-state.hpp"
#include "message-callback.hpp"
#include "sensor-data.hpp"

class PureCoolController : public MessageCallback
{
public :
  enum class INFO_TYPES
  {
    STATE = 0,
    SENSOR,
    FAULTS,
  };

  using PureCoolControllerFunctionBool = void (PureCoolController::*)(bool   );
  using PureCoolControllerFunctionInt  = void (PureCoolController::*)(int32_t);

  PureCoolController() = default;

  void subscribe_status();

  void update_info(INFO_TYPES info_type_);

  std::string get_description(INFO_TYPES info_type_, const std::string & key_) const;

protected :
  void _set_auto_on     (bool state_);
  void _set_forward_on  (bool state_);
  void _set_nightmode_on(bool state_);
  void _set_range       (int32_t lower_, int32_t range_);
  void _set_sleep_time  (int32_t value_);
  void _set_speed       (int32_t value_);
  void _set_state_on    (bool state_);

  void _set_fnsp_delta (int32_t delta_speed_);
  void _set_range_delta(int32_t delta_range_);

  void _send_data_bool_message(const std::string & field_, bool                value_);
  void _send_data_int_message (const std::string & field_, int32_t             value_);
  void _send_data_message     (const std::string & field_, const std::string & value_);

  void _toggle_state();

  std::string _command_topic;

  std::map<INFO_TYPES, std::shared_ptr<DataContainer> > _parser_map =
  {
    { INFO_TYPES::STATE , std::make_shared<DeviceState>() },
    { INFO_TYPES::SENSOR, std::make_shared<SensorData >() },
  };

  virtual void on_success(const mqtt::token & token_) override;

  virtual void message_arrived(mqtt::const_message_ptr message_) override;

private :
  PureCoolController            (const PureCoolController & other_) = delete;
  PureCoolController & operator=(const PureCoolController & other_) = delete;
};
